import { Component, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { Photo } from './model/photo';
import { Video, VideoFile, VideoResponse } from './model/video';

@Component({
  selector: 'acn-root',
  template: `

    <form #f="ngForm" (submit)="submit()">
      <input type="text" ngModel name="text">
      <select [(ngModel)]="type" name="type" (change)="changeType()">
        <option value="image">Images</option>
        <option value="video">Videos</option>    
      </select>
    </form>
    
    <div style="width: 100%; overflow: auto">
      <div style="display: flex">
        <div 
          *ngFor="let photo of images" 
          (click)="selectedImage = photo"
          class="item"
          [ngClass]="{
            'selectedItem': selectedImage?.id === photo.id
          }"
        >
          
          <img [src]="photo.src.tiny" width="100" alt="">
        </div>

        <div *ngFor="let video of videos" (click)="selectedVideo = video.video_files[0]">
          <img [src]="video.image" width="100" alt="">
        </div>
      </div>
    </div>
    
    <img 
      *ngIf="selectedImage"
      [src]="selectedImage?.src.large" alt="" width="100%">
    
    
    <ng-container *ngIf="selectedVideo">
      video metadata: {{selectedVideo?.width}} x {{selectedVideo?.height}}  ({{selectedVideo?.quality}})
      <video [src]="selectedVideo?.link" controls width="100%"></video>
    </ng-container>
  `,
  styles: [`
    .item {
      transition: 0.6s padding cubic-bezier(0.34, 1.56, 0.64, 1);
    }
    .selectedItem {
      padding: 0 20px;
    }
  `],
})
export class AppComponent {
  @ViewChild('f') form: NgForm;
  images: Photo[];
  videos: Video[];
  selectedImage: Photo;
  selectedVideo: VideoFile;
  type = 'image';

  constructor(private http: HttpClient) {}

  submit(): void {
    if (this.type === 'image') {
      this.searchImage();
    } else {
      this.searchVideo();
    }
  }

  searchImage(): void {
    this.http.get<any>(
      'https://api.pexels.com/v1/search?per_page=10&query=' + this.form.value.text,
      {
        headers: {
          Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
        }
      }
    )
      .subscribe(res => {
        this.images = res.photos;
        this.selectedImage = res.photos[0];
      });
  }

  searchVideo(): void {
    this.http.get<VideoResponse>(
      'https://api.pexels.com/videos/search?per_page=10&query=' + this.form.value.text,
      {
        headers: {
          Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be'
        }
      }
    )
      .subscribe(res => {
        this.videos = res.videos;

      });
  }

  changeType(): void {
    console.log('change type', this.type);
    this.images = null;
    this.videos = null;
    this.selectedImage = null;
    this.selectedVideo = null;
    this.submit();
  }
}
